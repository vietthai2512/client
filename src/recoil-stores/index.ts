import { atom } from 'recoil';
import { Authenticated, CallingAPI, DeviceInput } from './interfaces';
import { Device, User } from 'types';

export const authenticated = atom<Authenticated>({
  key: 'authenticated',
  default: {
    isLogin: false,
  },
});

export const callingAPIState = atom<CallingAPI>({
  key: 'callingAPIState',
  default: {
    loading: true,
    response: null,
  },
});

export const currentUser = atom<User | undefined>({
  key: 'currentUser',
  default: undefined,
});

export const deviceList = atom<Device[]>({
  key: 'deviceList',
  default: [],
});

export const deviceInput = atom<DeviceInput>({
  key: 'deviceInput',
  default: {
    step: 1,
    name: '',
    slug: '',
    image: '',
    files: [],
    attributes: [],
    fileList: [],
  },
  dangerouslyAllowMutability: true,
});
