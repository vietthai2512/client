import React, { memo, useCallback, useEffect, useState } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import { BlankLayout, HomeLayout, MainLayout } from 'layouts';

import { PrivateRoute, Spinner } from 'components';
import { AppRoute } from 'helpers';
import { AuthService, DeviceService } from 'services';
import { useRecoilState } from 'recoil';
import { authenticated, currentUser, deviceList } from 'recoil-stores';

import {
  HomePage,
  LoginPage,
  PageError404,
  PageError500,
  LogoutPage,
  SignupPage,
  DeviceFormPage,
  DeviceInstructionPage,
  DeviceDetailPage,
  AttributePage,
  UserDetail,
  AttributeEditPage,
  AttributeNewPage,
  BannerPage,
  VersionManagementPage,
  NewVersionPage,
  EditVerionPage,
  ReleaseList,
  ApiDoc,
  DashboardPage
} from 'pages';
import Users from 'pages/Users';
import { message } from 'antd';

export const RootRouter = memo(() => {
  const [, setCurrentUser] = useRecoilState(currentUser);
  const [, setDevices] = useRecoilState(deviceList);
  const [{ isLogin }, setLogin] = useRecoilState(authenticated);

  const [loading, setLoading] = useState(true);

  const request = useCallback(async () => {
    const token = localStorage.getItem('accessToken');

    if (token) {
      try {
        const { body: user } = await AuthService.checkMe(token);
        const { body: devices } = await DeviceService.getDeviceByUser();

        setDevices(devices);
        setCurrentUser(user);
        setLogin({ isLogin: true });
      } catch (error) {
        message.error('Oops! Something went wrong!');
      }
    }

    setLoading(false);
  }, [setCurrentUser, setDevices, setLogin]);

  useEffect(() => {
    request();
  }, [request]);

  if (loading) return <Spinner loading={loading}>{null}</Spinner>;

  return (
    <Switch>
      <Route path="/login" component={LoginPage} exact />
      <Route path={AppRoute.signup} component={SignupPage} />

      <PrivateRoute isLogin={isLogin} path={AppRoute.home} component={HomePage} exact layout={MainLayout} />
      <PrivateRoute isLogin={isLogin} path={AppRoute.logout} component={LogoutPage} exact layout={BlankLayout} />
      <PrivateRoute
        isLogin={isLogin}
        path={AppRoute.instruction}
        component={DeviceInstructionPage}
        exact
        layout={HomeLayout}
      />
      <PrivateRoute
        isLogin={isLogin}
        path={AppRoute.device_detail}
        component={DeviceDetailPage}
        exact
        layout={MainLayout}
      />
      <PrivateRoute isLogin={isLogin} path={AppRoute.new_device} component={DeviceFormPage} exact layout={MainLayout} />
      <PrivateRoute isLogin={isLogin} path={AppRoute.attribute} component={AttributePage} exact layout={MainLayout} />
      <PrivateRoute
        isLogin={isLogin}
        path={AppRoute.versions}
        component={VersionManagementPage}
        exact
        layout={MainLayout}
      />
      <PrivateRoute
        isLogin={isLogin}
        path={AppRoute.new_versions}
        component={NewVersionPage}
        exact
        layout={MainLayout}
      />
      <PrivateRoute
        isLogin={isLogin}
        path={AppRoute.edit_version}
        component={EditVerionPage}
        exact
        layout={MainLayout}
      />
      <PrivateRoute isLogin={isLogin} path={AppRoute.release_list} exact component={ReleaseList} layout={MainLayout} />
      <PrivateRoute
        isLogin={isLogin}
        path={AppRoute.edit_attribute}
        component={AttributeEditPage}
        exact
        layout={MainLayout}
      />
      <PrivateRoute
        isLogin={isLogin}
        path={AppRoute.new_attribute}
        component={AttributeNewPage}
        exact
        layout={MainLayout}
      />
      <PrivateRoute
        isLogin={isLogin}
        path={AppRoute.dashboard}
        component={DashboardPage}
        exact
        layout={MainLayout}
      />
      <PrivateRoute isLogin={isLogin} path={AppRoute.users} component={Users} exact layout={MainLayout} />
      <PrivateRoute isLogin={isLogin} path={AppRoute.banners} exact layout={MainLayout} component={BannerPage} />
      <PrivateRoute isLogin={isLogin} path={AppRoute.user_detail} component={UserDetail} exact layout={MainLayout} />

      <Route path={AppRoute.apis} component={ApiDoc} exact />

      <Route path="/404" component={PageError404} exact />
      <Route path="/500" component={PageError500} exact />
      <Redirect path="/" to="/" />
      <Redirect path="*" to="/404" />
    </Switch>
  );
});
