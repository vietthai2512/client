import React, { memo, SFC } from 'react';
import { Button } from 'antd';

import styles from '../styles/index.module.less';
import { useHistory } from 'react-router-dom';
import { AppRoute } from 'helpers';

const Error404: SFC = memo(() => {
  const history = useHistory();

  const goToHomePage = (e: React.MouseEvent) => {
    e.preventDefault();
    history.push(AppRoute.home);
  };
  return (
    <div className={styles.iso404Page}>
      <div className={styles.iso404Content}>
        <h1>404</h1>
        <h3>Looks like you got lost</h3>
        <p>The page your looking for doesnt exist or has been moved.</p>
        <Button type="primary" onClick={goToHomePage} shape="round" style={{ marginTop: 15 }}>
          Back home
        </Button>
      </div>
      <div className={styles.iso404Artwork}>
        <img alt="404" src="#" />
      </div>
    </div>
  );
});

export default Error404;
