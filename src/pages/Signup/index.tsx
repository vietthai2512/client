import { AppRoute } from 'helpers';
import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { useRecoilState } from 'recoil';
import { authenticated } from 'recoil-stores';
import { SignupContainer } from 'containers';

export interface SignupProps {}

const Signup: React.SFC<SignupProps> = () => {
  const [{ isLogin }] = useRecoilState(authenticated);

  const history = useHistory();

  useEffect(() => {
    if (isLogin) {
      history.push(AppRoute.home);
    }
  }, [history, isLogin]);

  return (
    <>
      <Helmet title="SIoT | Signup" />
      <SignupContainer />
    </>
  );
};

export default Signup;
