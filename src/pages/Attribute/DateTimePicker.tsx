import { DatePicker } from 'antd';
import React, { useEffect, useState } from 'react';
import moment from 'moment';

const { RangePicker } = DatePicker;

export interface DateTimePickerProps {
  onDateTimeChange: ([from, to]: [moment.Moment, moment.Moment]) => void;
}

const DateTimePicker: React.SFC<DateTimePickerProps> = ({ onDateTimeChange }) => {
  const [dates, setDates] = useState<[moment.Moment, moment.Moment]>([moment().startOf('day'), moment().endOf('day')]);

  useEffect(() => {
    onDateTimeChange(dates);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dates]);

  return <RangePicker showTime onChange={(values: any) => setDates(values)} value={dates} clearIcon={null} />;
};

export default DateTimePicker;
