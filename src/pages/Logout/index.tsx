import { AppRoute } from 'helpers';
import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useRecoilState } from 'recoil';
import { authenticated, currentUser } from 'recoil-stores';
import { AuthService } from 'services';

export default function Logout() {
  const history = useHistory();
  const [, setLogin] = useRecoilState(authenticated);
  const [, setUser] = useRecoilState(currentUser);

  useEffect(() => {
    const logout = async () => {
      try {
        AuthService.logout();
      } catch (error) {
        //
      } finally {
        localStorage.clear();
        setLogin({ isLogin: false });
        setUser(undefined);

        history.push(AppRoute.login);
      }
    };

    logout();
  }, [history, setLogin, setUser]);

  return <></>;
}
