import { Button, Col, Form, Input, Row } from 'antd';
import { Dropdown } from 'components';
import { AppRoute } from 'helpers';
import useValidateMessage from 'helpers/useValidatationMessages';
import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory, useParams } from 'react-router-dom';
import { useRecoilState } from 'recoil';
import { deviceList } from 'recoil-stores';
import { DeviceService, SlugService } from 'services';
import { Attribute, AttributeDataType } from 'types';
import { ErrorMessage } from 'utils';
import styles from './index.module.less';

export interface AttributeEditFormProps {}

const AttributesForm: React.SFC<AttributeEditFormProps> = () => {
  const [loading, setLoading] = useState(false);
  const [attribute, setAttribute] = useState<Attribute>();

  const form = Form.useForm()[0];

  const { attributeSlug, deviceSlug } = useParams<{ deviceSlug: string; attributeSlug: string }>();
  const [devices] = useRecoilState(deviceList);

  useEffect(() => {
    const fetch = async () => {
      try {
        const { body } = await DeviceService.getAttributeDetail(attributeSlug);
        setAttribute(body);
      } catch (error) {
        ErrorMessage();
      }
    };

    fetch();
  }, [attributeSlug]);

  const history = useHistory();
  const device = devices.find((d) => d.slug === deviceSlug);

  const handleSubmit = async (store: any) => {
    if (!store || !attribute) {
      return;
    }

    setLoading(true);
    try {
      await DeviceService.updateAttribute({ ...attribute, ...store, dataLabel: store.dataLabel });
      history.push(AppRoute.device_detail_ref(device?.slug || '') + '?tab=attributes');
      window.location.reload();
    } catch (error) {
      ErrorMessage();
    }
    setLoading(false);
  };

  const validateSlug = () => ({
    async validator(rule: any, value: any) {
      if (value) {
        if (value === attribute?.slug) {
          return Promise.resolve();
        }

        try {
          const {
            body: { verify },
          } = await SlugService.verify(value);

          if (!verify) {
            return Promise.reject('Slug has been used');
          }
        } catch (error) {
          ErrorMessage();
        }
      }

      return Promise.resolve();
    },
  });

  useEffect(() => {
    form.resetFields();
  }, [attribute, form]);

  const validateMessage = useValidateMessage();

  useEffect(() => {
    if (!devices || !devices.length || !devices.find((d) => d.slug === deviceSlug)) {
      history.push('/404');
    }
  }, [deviceSlug, devices, history]);

  return (
    <>
      <Helmet title={attribute?.name + '- Edit'} />
      <div className="device_tabs_content" style={{ margin: 'auto', width: 1200, marginTop: 24 }}>
        <div className={styles.info} style={{ marginBottom: 30 }}>
          <div>
            <h2 className="h2">Edit attribute:</h2>
          </div>
          <Row gutter={16}>
            <Col span={6}>
              <div className={styles.card}>
                <div>
                  <Row gutter={8}>
                    <Col>
                      <span className={styles.label}>Device:</span>
                    </Col>
                    <Col>
                      <span className={styles.txt}>{device?.name}</span>
                    </Col>
                  </Row>
                  <Row gutter={8}>
                    <Col>
                      <span className={styles.label}>Attribute:</span>
                    </Col>
                    <Col>
                      <span className={styles.txt}>{attribute?.name}</span>
                    </Col>
                  </Row>
                </div>
              </div>
            </Col>
            <Col span={18}>
              <Form
                name="attribute-form"
                form={form}
                initialValues={{
                  ...attribute,
                  dataType: attribute ? [attribute?.dataType] : [],
                }}
                onFinish={handleSubmit}
                validateMessages={validateMessage}
                layout="vertical"
              >
                <Form.Item name="name" label="Name:" rules={[{ required: true }]}>
                  <Input placeholder="Attribute Name" />
                </Form.Item>
                <Form.Item
                  name="slug"
                  rules={[
                    {
                      required: true,
                      whitespace: true,
                      message: 'Missing slug name!',
                    },
                    { whitespace: false },
                    {
                      pattern: new RegExp('^([a-zA-Z0-9\\u3131-\\uD79D-_.])*$'),
                      message: 'Slug name cannot have white space',
                    },
                    validateSlug,
                  ]}
                  label="Url slug name:"
                >
                  <Input placeholder="Slug name" />
                </Form.Item>

                <Form.Item
                  name="dataType"
                  label="Data type:"
                  rules={[{ required: true, message: 'Missing attribute type' }]}
                >
                  <Dropdown
                    options={[...Object.keys(AttributeDataType).map((attr) => ({ name: attr, code: attr }))]}
                    placeholder="Type"
                    disabled
                  />
                </Form.Item>

                <Form.Item
                  name="dataLabel"
                  rules={[
                    {
                      required: true,
                      whitespace: true,
                      message: 'Missing data label name!',
                    },
                    { whitespace: false },
                  ]}
                  label="Data label:"
                >
                  <Input placeholder="Data label: *C, PH, ..." />
                </Form.Item>

                <Button htmlType="submit" type="primary" style={{ width: 200 }} loading={loading} disabled={loading}>
                  Save
                </Button>
              </Form>
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
};

export default AttributesForm;
