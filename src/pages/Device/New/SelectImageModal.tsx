/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import { Col, message, Row, Typography, Upload } from 'antd';
import Modal from 'antd/lib/modal';
import React, { useState } from 'react';
import { CloseCircleSVG, UploadFileSVG } from 'icons';
import { PlusOutlined } from '@ant-design/icons';
import { Spinner } from 'components';
import { UploadMedia } from 'services';

const { Text } = Typography;

export interface SelectImageModalProps {
  files: any[];
  loading: boolean;
  setLoading: (loading: boolean) => void;
  fileList: string[];
  setFileList: (fileList: string[]) => void;
  onImageSelect: (val: string) => void;
  selectedImage?: string;
}

function SelectImageModal({
  files,
  loading,
  setLoading,
  onImageSelect,
  fileList,
  setFileList,
  selectedImage,
}: SelectImageModalProps) {
  const [visible, setVisible] = useState(false);

  return (
    <div style={{ marginTop: 24 }}>
      {selectedImage ? (
        <img
          src={selectedImage}
          alt="Selected avatar"
          className="text-center w-40 h-40 cursor-pointer"
          onClick={() => setVisible(true)}
        />
      ) : (
        <div
          className="text-center w-40 h-40 border border-dashed border-black-35 pt-12 hover:border-black-65 cursor-pointer"
          onClick={() => setVisible(true)}
        >
          <PlusOutlined />
          <div style={{ marginTop: 8 }}>Upload</div>
        </div>
      )}

      <Modal
        visible={visible}
        onCancel={() => setVisible(false)}
        closable={false}
        maskClosable={true}
        title={null}
        footer={null}
        bodyStyle={{ padding: 16 }}
      >
        <Row justify="space-between" className="border-black-8 border-b pb-2 mb-4">
          <Col>
            <Text strong>Select Device Avatar</Text>
          </Col>
          <Col>
            <CloseCircleSVG className="cursor-pointer" onClick={() => setVisible(false)} />
          </Col>
        </Row>
        <Row gutter={[12, 12]}>
          <Col>
            <VideoAndImageUploader fileList={fileList} setFileList={setFileList} />
          </Col>
          {fileList.map((file) => (
            <Col key={file}>
              <img
                src={file}
                alt="device-avatar"
                className="h-48 w-auto cursor-pointer"
                onClick={() => {
                  onImageSelect(file);
                  setVisible(false);
                }}
              />
            </Col>
          ))}
        </Row>
      </Modal>
    </div>
  );
}

export interface VideoAndImageUploaderProps {
  fileList: string[];
  setFileList: (fileList: string[]) => void;
}

function VideoAndImageUploader({ fileList, setFileList }: VideoAndImageUploaderProps) {
  const [loading, setLoading] = useState(false);

  const uploadButton = (
    <Row
      className="w-48 h-48 border-black-8 rounded border border-dashed bg-black-3 text-center flex-col hover:border-black-35"
      align="middle"
      justify="center"
    >
      {loading ? (
        <Spinner height={104} />
      ) : (
        <>
          <UploadFileSVG />
          <Text strong className="text-black-65">
            Upload
          </Text>
        </>
      )}
    </Row>
  );

  async function beforeUpload(file: File) {
    if (!file.type.includes('image')) {
      message.error('Only image file is allowed!');
      return;
    }

    setLoading(true);

    try {
      const media = await UploadMedia(file as any);

      const url = await media.ref.getDownloadURL();

      setFileList([url, ...fileList]);
    } catch (error) {
      message.error('Oops! Error when uploading file. Try later!');
    }

    setLoading(false);

    return false;
  }

  return (
    <Upload
      // action={process.env.REACT_APP_UPLOAD_ENDPOINT}
      accept="image/*"
      showUploadList={false}
      beforeUpload={(file) => {
        beforeUpload(file);
        return false;
      }}
    >
      {uploadButton}
    </Upload>
  );
}

export default SelectImageModal;
