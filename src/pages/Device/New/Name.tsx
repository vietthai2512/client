import { Button, Col, Form, Row } from 'antd';
import { AppInput, DeviceCard, FormItem } from 'components';
import React from 'react';
import { useRecoilState } from 'recoil';
import { deviceInput } from 'recoil-stores';
import { SlugService } from 'services';

export interface NameProps {}

const Name: React.SFC<NameProps> = () => {
  const [{ name, image, step, slug }, setState] = useRecoilState(deviceInput);
  const [form] = Form.useForm();

  const handleFieldChange = async (fields: any) => {
    console.log(fields[0]);

    if (fields[0] && fields[0].name[0] === 'name' && fields[0].value) {
      console.log(1);
      try {
        const {
          body: { slug },
        } = await SlugService.suggest({ name: fields[0].value, type: 'DEVICE' });

        form.setFieldsValue({
          slug,
        });
      } catch (error) {
        //
      }
    }
  };

  const validateSlug = () => ({
    async validator(rule: any, value: any) {
      if (value) {
        try {
          const {
            body: { verify },
          } = await SlugService.verify(value);

          if (!verify) {
            return Promise.reject('Slug has been used');
          }
        } catch (error) {
          //
        }
      }

      return Promise.resolve();
    },
  });

  const handleSubmit = () => {
    const { name, slug } = form.getFieldsValue();

    setState((state) => ({ ...state, name, slug, step: step + 1 }));
  };

  return (
    <Form
      name="device-name"
      form={form}
      initialValues={{ name, slug }}
      onFieldsChange={handleFieldChange}
      onFinish={handleSubmit}
    >
      <Row className="device-form_form-group">
        <Col span={12}>
          <Col span={14} className="form-label">
            <h2 className="h2">{`Let's start with a name for your device`}</h2>
          </Col>
          <Col span={18}>
            <FormItem name="name" rules={[{ required: true }]}>
              <AppInput placeholder="Device name" className="form-control" />
            </FormItem>
            <FormItem
              name="slug"
              rules={[
                { required: true },
                { whitespace: false },
                {
                  pattern: new RegExp('^([a-zA-Z0-9\\u3131-\\uD79D-_.])*$'),
                  message: 'Slug name cannot have white space',
                },
                validateSlug,
              ]}
            >
              <AppInput placeholder="Slug name" className="form-control" />
            </FormItem>
          </Col>
        </Col>

        <Col span={8}>
          <DeviceCard device={{ image, name, slug }} allowAction={false} />
        </Col>

        <Col span={24}>
          <Row className="device-form_button-action">
            <Col span={12} className="next-button">
              <Button type="primary" htmlType="submit">
                Continue
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </Form>
  );
};

export default Name;
