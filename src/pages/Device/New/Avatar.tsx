import { Button, Col, Row } from 'antd';
import React, { useEffect, useState } from 'react';

import { DeviceCard } from 'components';
import { BannerService } from 'services';
import { useRecoilState } from 'recoil';
import { deviceInput } from 'recoil-stores';
import SelectImageModal from './SelectImageModal';

export interface AvatarProps {}

const Avatar: React.SFC<AvatarProps> = () => {
  const [loading, setLoading] = useState(false);
  const [{ name, image, step, files, slug, fileList }, setState] = useRecoilState(deviceInput);

  useEffect(() => {
    const fetch = async () => {
      try {
        const data = await BannerService.getAllPaging(1, 1000);
        setState((state) => ({ ...state, fileList: data.body.map((b) => b.url) }));
      } catch (error) {
        //
      }
    };

    if (!fileList.length) {
      fetch();
    }
  }, [fileList.length, setState]);

  return (
    <div>
      <Row className="device-form_form-group">
        <Col span={12}>
          <Col span={24} className="form-label">
            <h2 className="h2">{`Add your device avatar`}</h2>
          </Col>

          <SelectImageModal
            files={files}
            loading={loading}
            setLoading={setLoading}
            fileList={fileList}
            setFileList={(fileList) => setState((state) => ({ ...state, fileList }))}
            onImageSelect={(url) => setState((state) => ({ ...state, image: url }))}
            selectedImage={image}
          />
        </Col>

        <Col span={8}>
          <DeviceCard device={{ image, name, slug }} allowAction={false} />
        </Col>

        <Col span={24}>
          <Row className="device-form_button-action">
            <Col span={12} className="prev-button">
              <Button type="text" onClick={() => setState((state) => ({ ...state, step: step - 1 }))}>
                Previous
              </Button>
            </Col>
            <Col span={12} className="next-button">
              <Button
                type="primary"
                onClick={(state) => setState((state) => ({ ...state, step: step + 1 }))}
                disabled={!image}
                loading={loading}
              >
                Continue
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default Avatar;
