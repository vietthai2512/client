import React, { useState, useEffect } from 'react';
import { Device } from 'types';
import { EditOutlined } from '@ant-design/icons';
import { Row, Col, Input } from 'antd';
import Button from 'antd/es/button/button';
import { Color, ErrorMessage } from 'utils';
import { DeviceService } from 'services';
import { useRecoilState } from 'recoil';
import { deviceList } from 'recoil-stores';

export interface DeviceNameProps {
  device?: Device;
  onDeviceChange: (device: Device) => void;
  disable?: boolean;
}

const DeviceName: React.SFC<DeviceNameProps> = ({ device, onDeviceChange, disable }) => {
  const [value, setValue] = useState('');
  const [editVisible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);

  const [, setDevices] = useRecoilState(deviceList);

  useEffect(() => {
    setValue(device?.name || '');
  }, [device]);

  const handleSave = async () => {
    setLoading(true);

    try {
      if (device) {
        const {
          body: { name },
        } = await DeviceService.editDeviceOwner(device.slug, { name: value });
        onDeviceChange({ ...device, name });
        setDevices((devices) => devices.map((d) => (d.slug === device.slug ? { ...device, name } : d)));
        setVisible(false);
        window.location.reload();
      }
    } catch (e) {
      ErrorMessage();
    }
    setLoading(false);
  };

  return (
    <Row className="card-option" align="middle">
      {editVisible && (
        <Col span={24}>
          <div style={{ position: 'absolute', width: '100%', backgroundColor: Color.WHITE, zIndex: 1 }}>
            <div>
              <Input className="form-control" value={value} onChange={(e) => setValue(e.target.value)} />
            </div>
            <div>
              <Row justify="end" gutter={[8, 0]} style={{ marginTop: 12 }}>
                <Col>
                  <Button
                    type="default"
                    size="small"
                    style={{ width: 60, height: 30 }}
                    onClick={() => setVisible(false)}
                  >
                    Cancel
                  </Button>
                </Col>
                <Col>
                  <Button
                    type="primary"
                    size="small"
                    style={{ width: 60, height: 30 }}
                    onClick={handleSave}
                    loading={loading}
                    disabled={loading}
                  >
                    Save
                  </Button>
                </Col>
              </Row>
            </div>
          </div>
        </Col>
      )}

      <Col className="card-label" span={6}>
        Device name
      </Col>
      <Col>
        <span className="active">
          {device?.name}{' '}
          {!disable && (
            <EditOutlined
              style={{ cursor: 'pointer' }}
              onClick={() => {
                setVisible(true);
                setValue(device?.name || '');
              }}
            />
          )}
        </span>
      </Col>
    </Row>
  );
};

export default DeviceName;
