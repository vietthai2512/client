import Table, { ColumnsType } from 'antd/lib/table';
import React from 'react';
import { Device } from 'types';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { AppRoute } from 'helpers';

export interface DeviceListProps {
  devices: Device[];
}

const DeviceList: React.SFC<DeviceListProps> = ({ devices }) => {
  const columns: ColumnsType<Device> = [
    {
      title: '',
      dataIndex: 'no',
      width: 70,
      render: (text, record, index) => <span>{index + 1}</span>,
    },
    {
      title: `Name`,
      dataIndex: 'name',
      render: (text, record) => <Link to={AppRoute.device_detail_ref(record.slug)}>{record.name}</Link>,
    },
    {
      title: `Slug name`,
      dataIndex: 'slug',
    },
    {
      title: 'Created date',
      dataIndex: 'created_at',
      render: (text, record) => (
        <span>{record?.createdAt ? moment(record.createdAt).format('DD/MM/YYYY HH:mm:ss') : ''}</span>
      ),
    },
  ];

  return <Table rowKey={(record) => record?.id || ''} dataSource={devices} columns={columns} />;
};

export default DeviceList;
