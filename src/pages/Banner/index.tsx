import { Row, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { BannerService } from 'services';
import { Banner as BannerEntity } from 'types';
import { CloseCircleSVG } from 'icons';
import UploadSingleImage from './UploadSingleImage';
import { motion, Variants } from 'framer-motion';

export interface BannerProps {}

const Banner: React.SFC<BannerProps> = () => {
  const [fileList, setFileList] = useState<BannerEntity[]>([]);
  const [deletedList, setDeletedList] = useState<BannerEntity[]>([]);

  useEffect(() => {
    const fetch = async () => {
      try {
        const data = await BannerService.getAllPaging(1, 1000);
        setFileList(data.body);
      } catch (error) {
        //
      }
    };

    fetch();
  }, []);

  const handleDelete = async (file: BannerEntity) => {
    // eslint-disable-next-line no-restricted-globals
    if (confirm('Are you sure you want to delete this banner?')) {
      await BannerService.deleteBanner(file.id);
      setDeletedList([...deletedList, file]);
    }
  };

  const variants: Variants = {
    visible: {
      opacity: 1,
    },
    hidden: {
      opacity: 0,
      width: 0,
      height: 0,
    },
  };

  return (
    <div>
      <Typography.Text className="text-2xl">SIoT Platform Release Version history</Typography.Text>
      <div className="my-4">
        <UploadSingleImage fileList={fileList} setFileList={setFileList} />
      </div>
      <Row>
        {fileList.map((f) => (
          <motion.div
            key={f.id}
            variants={variants}
            initial="visible"
            animate={deletedList.find((d) => d.id === f.id) ? 'hidden' : 'visible'}
            transition={{ duration: 0.5 }}
            className={`relative ${deletedList.find((d) => d.id === f.id) ? '' : 'mr-4 mb-4'}`}
          >
            <img src={f.url} alt="device banner" className="h-48 w-auto" />
            <CloseCircleSVG className="absolute -top-3 -right-3 cursor-pointer" onClick={() => handleDelete(f)} />
          </motion.div>
        ))}
      </Row>
    </div>
  );
};

export default Banner;
