import React, { memo, useEffect } from 'react';
import { Helmet } from 'react-helmet';

function createIframe() {
    return {__html: '<iframe src="http://localhost:3000/d-solo/r84h-qgnk/siot-dashboard?orgId=2&from=1592896603014&to=1624432603014&panelId=2" width="450" height="200" frameborder="0"></iframe>'};
  }

const DashboardPage: React.SFC = React.memo(() => {    
    return (
        <div>
            <div dangerouslySetInnerHTML={createIframe()} />
        </div>
    );
  });

export default DashboardPage;
