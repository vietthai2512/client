import React from 'react';
import { Device } from 'types';

export interface DeviceCardProps {
  device: Device;
  allowAction?: boolean;
}

const DeviceCard: React.SFC<DeviceCardProps> = ({ device: { name, image, slug }, allowAction = true }) => {
  return (
    <div className="device-card">
      <div className="device-card_item" style={{ cursor: allowAction ? 'pointer' : 'unset' }}>
        {image && <img src={image} alt="device-avatar" className="device-card_avatar" />}
        <div style={{ zIndex: 2 }}>
          <h4 className="h4">{name}</h4>
          <span>{slug}</span>
        </div>
      </div>
    </div>
  );
};

export default DeviceCard;
