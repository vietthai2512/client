import React from 'react';
import { Spin } from 'antd';
import { SpinSize } from 'antd/lib/spin';

export interface SpinnerProps {
  height?: number | string;
  loading?: boolean;
  size?: SpinSize;

  render?: any;
  children?: any;
}

const Spinner: React.SFC<SpinnerProps> = ({ height, loading = true, size, render, children }) => {
  return loading ? (
    <Spin
      style={{
        width: '100%',
        height: height || 200,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
      spinning={loading}
      size={size || 'default'}
    >
      {render || children}
    </Spin>
  ) : (
    render || children
  );
};

export default Spinner;
