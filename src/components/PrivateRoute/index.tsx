import React from 'react';
import { Route, RouteProps, Redirect } from 'react-router-dom';
import { AppRoute } from 'helpers';

interface PrivateRouteProps extends RouteProps {
  [x: string]: any;
  component: React.FC<any>;
  isLogin: boolean;
  layout: React.FC<any>;
}

const PrivateRoute = ({ component: Component, layout: Layout, isLogin, ...rest }: PrivateRouteProps): JSX.Element => {
  return (
    <Route
      {...rest}
      render={(props) =>
        isLogin ? (
          <Layout>
            <Component {...props} />{' '}
          </Layout>
        ) : (
          <Redirect to={AppRoute.login + '?redirect=' + rest.location?.pathname} />
        )
      }
    />
  );
};
export default PrivateRoute;
