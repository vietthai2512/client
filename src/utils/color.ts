const HOT = '#ff4d4f';
const WHITE = '#FFFFFF';
const PRIMARY = '#003766';
const DARK_GRAY = '#0000008a';
const PINK = '#f27173';

export default { HOT, WHITE, PRIMARY, DARK_GRAY, PINK };
