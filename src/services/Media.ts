import { RcFile } from 'antd/lib/upload';
import firebase from 'firebase';

export default async (image: RcFile) => {
  const storageRef = firebase.storage().ref('images/' + new Date().toISOString() + '-' + image.name);

  return await storageRef.put(image);
};
