export { default as AuthService } from './Auth';
export { default as UploadMedia } from './Media';
export { default as DeviceService } from './Device';
export { default as SlugService } from './Slug';
export { default as UserService } from './User';
export { default as BannerService } from './Banner';
export { default as PlatformVersionService } from './PlatformVersion';
