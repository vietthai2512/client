export { ReactComponent as Camera } from './camera.svg';
export { ReactComponent as UploadFileSVG } from './upload.svg';
export { ReactComponent as CloseCircleSVG } from './close-circle.svg';
