FROM node:14-alpine as client-builder
WORKDIR /usr/src/client
COPY . .
RUN npm install -force && npm install eslint@^6.6.0
RUN npm run build

FROM node:14-alpine as client-production
WORKDIR /usr/src/client
COPY --from=client-builder ./usr/src/client/build ./build
RUN npm install -g serve
EXPOSE 90
CMD ["serve", "-s", "build", "-l", "90"]